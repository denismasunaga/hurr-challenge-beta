class SendBlockedDateWorker
  include Sidekiq::Worker
  sidekiq_options retry: 5
  sidekiq_options queue: :beta_default

  def perform(blocked_dates, user_email, create)
    SynchronizationService::SendBlockedDates.run(blocked_dates: blocked_dates, user_email: user_email, create: create)
  end
end
