module BlockedDateService
  class SplitDate
    def self.run(split_string = ' to ',params:)
      new(split_string: split_string, params: params).run
    end

    def run
      split_dates
    end

    private

    def initialize(split_string:, params:)
      @split_string = split_string
      @params = params
    end

    def split_dates
      dates_array = @params[:date_range].split @split_string

      @params[:starting_date] = dates_array[0]
      @params[:ending_date] = dates_array[1].present? ? dates_array[1] : dates_array[0]

      @params.except(:date_range)
    end
  end
end