module AuthTokenService
  class AuthTokenValidate
    def self.run(token:)
      new(token: token).run
    end

    def run
      validate_token
    end

    private

    def initialize(token:)
      @token = token
    end

    def validate_token
      @token == ENV['API_SECRET_KEY']
    end
  end
end
