module SynchronizationService
  class ProcessBlockedDates
    def self.run(blocked_dates:, user_id:)
      new(blocked_dates: blocked_dates, user_id: user_id).run
    end

    def run
      process_blocked_dates
    end

    private

    def initialize(blocked_dates:, user_id:)
      @blocked_dates = blocked_dates
      @user_id = user_id
    end

    def process_blocked_dates
      return [] if @blocked_dates.blank?

      blocked_dates = @blocked_dates['blocked_dates'].each do |h|
        h.delete('id')
        h['user_id'] = @user_id
      end

      BlockedDate.create!(blocked_dates)
    rescue StandardError => e
      nil
    end
  end
end