module SynchronizationService
  class DestroyBlockedDates
    def self.run(blocked_dates:, user_id:)
      new(blocked_dates: blocked_dates, user_id: user_id).run
    end

    def run
      process_blocked_dates
    end

    private

    def initialize(blocked_dates:, user_id:)
      @blocked_dates = blocked_dates
      @user_id = user_id
    end

    def process_blocked_dates
      return [] if @blocked_dates.blank?

      starting_dates = []
      ending_dates = []
      @blocked_dates['blocked_dates'].each do |h|
        starting_dates << h['starting_date']
        ending_dates << h['ending_date']
      end

      blocked_dates_ids = BlockedDate.where(user_id: @user_id, starting_date: starting_dates, ending_date: ending_dates).pluck(:id)

      BlockedDate.destroy(blocked_dates_ids)
    rescue StandardError => e
      nil
    end
  end
end