module Api
  module V1
    class SynchronizationController < ApiBaseController
      before_action :validate_token

      def get_user_blocked_dates
        user = get_user

        if user.present?
          render json: {
            blocked_dates: user.blocked_dates.select(:id, :starting_date, :ending_date)
          }
        else
          render json: { message: 'Invalid token' }, status: :unprocessable_entity
        end
      end

      def send_user_blocked_dates
        user = get_user
        StoreBlockedDateWorker.perform_async(blocked_dates_params.to_h, user.id)

        render json: {}, status: :ok
      end

      def destroy_user_blocked_dates
        user = get_user
        DestroyBlockedDateWorker.perform_async(blocked_dates_params.to_h, user.id)
      end

      private

      def blocked_dates_params
        params.permit(:email, blocked_dates: [:id, :starting_date, :ending_date])
      end

      def get_user
        User.find_by(email: blocked_dates_params[:email])
      end

    end
  end
end
