require 'sidekiq/web'

Rails.application.routes.draw do
  resources :blocked_dates

  mount Sidekiq::Web => "/sidekiq" # mount Sidekiq::Web in your Rails app

  namespace :api do
    namespace :v1 do
      post 'get-user-blocked-dates', to: 'synchronization#get_user_blocked_dates'
      post 'send-user-blocked-dates', to:  'synchronization#send_user_blocked_dates'
      post 'destroy-user-blocked-dates', to:  'synchronization#destroy_user_blocked_dates'
    end
  end

  devise_for :users

  root to: 'blocked_dates#index'
end
